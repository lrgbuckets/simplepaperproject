﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This exists just so we don't need to copy all the surface data between multiple render managers
/// </summary>
public class SurfaceList : MonoBehaviour
{
    /// <summary>
    /// A CompleteSurface is made from one or more segments
    /// </summary>
    public CompleteSurfaceInitData[] Surfaces;
}
