﻿Shader "AFoldApart/PaperProjection/BlitAccumulationToScreen"
{
	// Command buffers write to global keywords so "white" will always take priority because it is more specific than the global
	//Properties
	//{
	//	_MainTex ("Texture", 2D) = "white" {}
	//}
	SubShader
	{
		Tags{ "Queue" = "Geometry" "PreviewType" = "Plane" }

		CGINCLUDE
#pragma enable_d3d11_debug_symbols
		#pragma target 3.0
#include "UnityCG.cginc"

		struct appdata
		{
			float3 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		sampler2D _MainTex;
		float4 _MainTex_ST;
		uniform float4 _MainTex_TexelSize;
		//sampler2D _MainDepth; //the depth texture

		v2f vert(appdata v)
		{
			v2f o;
			o.vertex = float4(v.vertex, 1);
			o.uv = v.uv;
#ifndef UNITY_UV_STARTS_AT_TOP
			o.uv.y = 1 - o.uv.y;
#endif
			return o;
		}

		fixed4 frag(v2f i, out float out_depth : SV_Depth) : SV_Target
		{
			//out_depth = UNITY_SAMPLE_DEPTH(tex2D(_MainDepth, i.uv));
            out_depth = 0;
			return tex2D(_MainTex, i.uv);
		}
		ENDCG

		Pass
		{
			ZWrite on
			ZTest off
			Cull back

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}
	}
}
