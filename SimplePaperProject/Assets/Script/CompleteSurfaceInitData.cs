﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering;

[System.Serializable]
public class CompleteSurfaceInitData
{
    [SerializeField]
    private MeshRenderer m_surfaceToDrawToScreen;

    [SerializeField]
    private GameObject m_surfaceToRenderToTexture;

    [SerializeField]
    private SurfaceSegmentInitData[] m_surfaceSegmentsToRenderToTexture;

    public MeshRenderer SurfaceToDrawToScreen
    {
        get { return m_surfaceToDrawToScreen; }
    }

    public GameObject SurfaceToRenderToTexture
    {
        get { return m_surfaceToRenderToTexture; }
    }

    public SurfaceSegmentInitData[] SurfaceSegmentsToRenderToTexture
    {
        get { return m_surfaceSegmentsToRenderToTexture; }
    }
}
