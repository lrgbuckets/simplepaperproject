﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// Uses a scaled backbuffer
/// </summary>
public class CameraMgr6 : MonoBehaviour
{
    private static readonly float TEXTURE_SCALE = 0.5f;
    private static readonly Vector2 TEXTURE_SCALE_SCREEN_FIX = new Vector2( 2f, 2f );
    private static readonly Vector2 TEXTURE_OFFSET_SCREEN_FIX = new Vector2( 0f, 0f );

    private static readonly CameraEvent BLIT_CAMERA_EVENT = CameraEvent.AfterEverything;
    private static readonly string BLIT_CAMERA_BUFFER_NAME = "Blit Scaled To Screen";

    [SerializeField]
    private Camera m_mainCamera;

    [SerializeField]
    private Camera m_screenBlitCamera;

    [SerializeField]
    private Camera m_screenOverlayCamera;

    [SerializeField]
    private SurfaceList m_surfaceList;

    [SerializeField]
    private ForwardPaperProjectionMaterials m_materials;

    private CompleteSurface[] m_surfaces;

    private RenderTexture m_offScreenBuffer;
    private Vector2Int m_lastScreenSize;

    private CommandBuffer m_blitScaledToScreen;
    private Mesh m_viewVolumeMesh;

    public static Mesh GenerateViewVolumeMesh()
    {
        Mesh viewVolumeMesh = new Mesh();
        viewVolumeMesh.vertices = new Vector3[] {
                new Vector3( -1f, 1f, 0f ),
                new Vector3( 1f, 1f, 0f ),
                new Vector3( -1f, -1f, 0f ),
                new Vector3( -1f, -1f, 0f ),
                new Vector3( 1f, 1f, 0f ),
                new Vector3( 1f, -1f, 0f ) };

        viewVolumeMesh.uv = new Vector2[] {
                new Vector2( 0f, 0f ),
                new Vector2( 1f, 0f ),
                new Vector2( 0f, 1f ),
                new Vector2( 0f, 1f ),
                new Vector2( 1f, 0f ),
                new Vector2( 1f, 1f ) };

        //viewVolumeMesh.triangles = new int[] { 0, 1, 2, 3, 4, 5 };
        viewVolumeMesh.triangles = new int[] { 5, 4, 3, 2, 1, 0 };

        viewVolumeMesh.RecalculateBounds();
        viewVolumeMesh.RecalculateNormals();

        return viewVolumeMesh;
    }

    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public static Vector2Int GetScaledScreenSize()
    {
        return new Vector2Int( (int)( TEXTURE_SCALE * Screen.width ), (int)( TEXTURE_SCALE * Screen.height ) );
    }

    private void Awake()
    {
        if( Application.isPlaying )
        {
            Application.targetFrameRate = 20;

            m_viewVolumeMesh = GenerateViewVolumeMesh();

            m_blitScaledToScreen = new CommandBuffer();
            m_blitScaledToScreen.name = BLIT_CAMERA_BUFFER_NAME;

            m_screenBlitCamera.AddCommandBuffer( BLIT_CAMERA_EVENT, m_blitScaledToScreen );

            int numSurfaces = m_surfaceList.Surfaces.Length;
            m_surfaces = new CompleteSurface[ numSurfaces ];
            for( int iSurface = 0; iSurface < numSurfaces; ++iSurface )
            {
                m_surfaces[ iSurface ] = new CompleteSurface( m_surfaceList.Surfaces[ iSurface ], m_materials );
            }

            Camera.onPreCull += OnCameraPreCull;
            Camera.onPostRender += OnCameraPostRender;
        }
    }

    private void OnDestroy()
    {
        Camera.onPreCull -= OnCameraPreCull;
        Camera.onPostRender -= OnCameraPostRender;
    }

    private void LateUpdate()
    {
        RefreshScreenBuffer();

        float depth = 0f;
        int numSurfaces = m_surfaces.Length;
        for( int iSurface = 0; iSurface < numSurfaces; ++iSurface )
        {
            CompleteSurface surface = m_surfaces[ iSurface ];

            depth = surface.RefreshForFrameStart( m_mainCamera, depth, m_offScreenBuffer );
        }

        m_mainCamera.depth = depth++;
        m_mainCamera.targetTexture = m_offScreenBuffer;

        m_screenBlitCamera.depth = depth++;

        m_blitScaledToScreen.Clear();

        m_blitScaledToScreen.SetGlobalTexture( "_MainTex", m_offScreenBuffer );
        m_blitScaledToScreen.DrawMesh( m_viewVolumeMesh, Matrix4x4.identity, m_materials.BlitToScreenUsingMesh );

        m_screenOverlayCamera.depth = depth;


    }

    private void RefreshScreenBuffer()
    {
        if( null == m_offScreenBuffer || GetScaledScreenSize() != m_lastScreenSize )
        {
            if( null != m_offScreenBuffer )
            {
                m_offScreenBuffer.Release();
            }

            m_lastScreenSize = GetScaledScreenSize();

            m_offScreenBuffer = new RenderTexture( m_lastScreenSize.x, m_lastScreenSize.y, 24 );
            m_offScreenBuffer.name = "OffScreenBuffer";
            m_offScreenBuffer.useMipMap = false;
            m_offScreenBuffer.useDynamicScale = false;
            m_offScreenBuffer.antiAliasing = 1;
        }
    }

    private void OnCameraPreCull( Camera cam )
    {
        SurfaceSegment passData = GetPassData( cam );
        if( null != passData )
        {
            passData.OnPassPreCull();
        }
    }

    private void OnCameraPostRender( Camera cam )
    {
        SurfaceSegment passData = GetPassData( cam );
        if( null != passData )
        {
            passData.OnPassPostRender();
        }
    }

    private SurfaceSegment GetPassData( Camera cam )
    {
        int numSurfaces = m_surfaces.Length;
        for( int iSurface = 0; iSurface < numSurfaces; ++iSurface )
        {
            CompleteSurface surface = m_surfaces[ iSurface ];

            int numSegments = surface.Segments.Length;
            for( int iSegment = 0; iSegment < numSegments; ++iSegment )
            {
                SurfaceSegment data = surface.Segments[ iSegment ];
                if( ReferenceEquals( cam, data.Camera ) )
                {
                    return data;
                }
            }
        }

        return null;
    }

    private class CompleteSurface
    {
        private static readonly Color CLEAR_COLOUR = new Color( 0f, 0f, 0f, 1f );

        public static class Uniforms
        {
            internal static readonly int _MainTex = Shader.PropertyToID( "_MainTex" );
            internal static readonly int _UvToWorld = Shader.PropertyToID( "_UvToWorld" );
        }

        private MeshRenderer m_surfaceToDrawToScreen;

        private GameObject m_surfaceToRenderToTexture;

        private SurfaceSegment[] m_surfaceSegmentsToRenderToTexture;

        private RenderTexture m_accumulationBuffer;

        private Vector2Int m_lastScreenSize;

        public CompleteSurface( CompleteSurfaceInitData initData, ForwardPaperProjectionMaterials materials )
        {
            m_surfaceToDrawToScreen = initData.SurfaceToDrawToScreen;
            m_surfaceToRenderToTexture = initData.SurfaceToRenderToTexture;

            int numSegments = initData.SurfaceSegmentsToRenderToTexture.Length;
            m_surfaceSegmentsToRenderToTexture = new SurfaceSegment[ numSegments ];
            for( int iSegment = 0; iSegment < numSegments; ++iSegment )
            {
                m_surfaceSegmentsToRenderToTexture[ iSegment ] = new SurfaceSegment( initData.SurfaceSegmentsToRenderToTexture[ iSegment ], iSegment, materials );
            }
        }

        public SurfaceSegment[] Segments
        {
            get { return m_surfaceSegmentsToRenderToTexture; }
        }

        /// <summary>
        /// Make sure that we have an accumulation texture of the correct size, clear it, then move the segment cameras to line up with the main camera
        /// </summary>
        public float RefreshForFrameStart( Camera mainCamera, float depth, RenderTexture offScreenBuffer )
        {
            RefreshTexture();

            ClearAccumulationTexture();

            Matrix4x4 relativeTransform = m_surfaceToRenderToTexture.transform.localToWorldMatrix * m_surfaceToDrawToScreen.transform.worldToLocalMatrix * mainCamera.transform.localToWorldMatrix;

            int numSegments = m_surfaceSegmentsToRenderToTexture.Length;
            for( int iSegment = 0; iSegment < numSegments; ++iSegment )
            {
                SurfaceSegment data = m_surfaceSegmentsToRenderToTexture[ iSegment ];

                data.RefreshForFrameStart( mainCamera, relativeTransform, depth, m_accumulationBuffer, offScreenBuffer );
                depth += 1f;
            }

            RefreshCompleteSurfaceMaterial();

            return depth;
        }

        private static void ExtractRotationFromMatrix( Matrix4x4 mat, out Quaternion rotation )
        {
            rotation = Quaternion.LookRotation( mat.GetColumn( 2 ), mat.GetColumn( 1 ) );
        }

        /// <summary>
        /// Calculate UV offset and scale that allows us to map the screen-size texture to the visible geometry
        /// </summary>
        private void RefreshCompleteSurfaceMaterial()
        {
            Bounds worldRenderBounds = m_surfaceToDrawToScreen.bounds;
            //Matrix4x4 localToWorld = m_surfaceToRenderToTexture.transform.localToWorldMatrix;

            Vector3 worldUvOrigin = worldRenderBounds.min;
            Vector3 worldUvX = ( worldRenderBounds.min + worldRenderBounds.size.x * Vector3.right ) - worldUvOrigin;
            Vector3 worldUvY = ( worldRenderBounds.min + worldRenderBounds.size.y * Vector3.up ) - worldUvOrigin;
            Vector3 worldZ = Vector3.forward;

            Vector4 originTransformColumn = new Vector4( worldUvOrigin.x, worldUvOrigin.y, worldUvOrigin.z, 1f );

            Matrix4x4 uvToWorld = new Matrix4x4( (Vector4)worldUvX, (Vector4)worldUvY, (Vector4)worldZ, originTransformColumn );

            m_surfaceToDrawToScreen.material.SetTexture( Uniforms._MainTex, m_accumulationBuffer );
            m_surfaceToDrawToScreen.material.SetMatrix( Uniforms._UvToWorld, uvToWorld );
        }

        private void ClearAccumulationTexture()
        {
            GL.PushMatrix();
            Graphics.SetRenderTarget( m_accumulationBuffer );
            GL.Clear( true, true, CLEAR_COLOUR );
            Graphics.SetRenderTarget( null );
            GL.PopMatrix();
        }

        private void RefreshTexture()
        {
            if( null == m_accumulationBuffer || GetScaledScreenSize() != m_lastScreenSize )
            {
                if( null != m_accumulationBuffer )
                {
                    m_accumulationBuffer.Release();
                }

                m_lastScreenSize = GetScaledScreenSize();

                m_accumulationBuffer = new RenderTexture( m_lastScreenSize.x, m_lastScreenSize.y, 16 );
                m_accumulationBuffer.name = "PaperSurfaceColour";
                m_accumulationBuffer.useMipMap = false;
                m_accumulationBuffer.useDynamicScale = false;
                m_accumulationBuffer.antiAliasing = 1;
            }
        }
    }

    private class SurfaceSegment
    {
        private static readonly CameraEvent SEGMENT_CAMERA_ACCUMULATE_EVENT = CameraEvent.AfterEverything;
        private static readonly string SEGMENT_CAMERA_ACCUMULATE_BUFFER_NAME = "Segment Accumulate";

        private static readonly CameraEvent SEGMENT_CAMERA_STENCIL_MASK_EVENT = CameraEvent.BeforeForwardOpaque;
        private static readonly string SEGMENT_CAMERA_STENCIL_MASK_BUFFER_NAME = "Segment Stencil Mask";

        public static class Uniforms
        {
            internal static readonly int _MainTex = Shader.PropertyToID( "_MainTex" );
            internal static readonly int _MainDepth = Shader.PropertyToID( "_MainDepth" );
            internal static readonly int _InputTexWidth = Shader.PropertyToID( "_InputTexWidth" );
            internal static readonly int _InputTexHeight = Shader.PropertyToID( "_InputTexHeight" );
        }

        private Camera m_camera;
        private AmplifyColorEffect m_gradeAndCopy;

        private GameObject m_drawRoot;

        private TestMesh m_mask;

        private CommandBuffer m_segmentStencilMask;
        private CommandBuffer m_segmentAccumulate;

        private Mesh m_maskMesh;

        private RenderTexture m_accumulationBuffer;
        private RenderTexture m_offScreenBuffer;
        private ForwardPaperProjectionMaterials m_renderMaterials;

        public SurfaceSegment( SurfaceSegmentInitData initData, int index, ForwardPaperProjectionMaterials renderMaterials )
        {
            m_camera = initData.Camera;
            m_drawRoot = initData.DrawRoot;
            m_mask = initData.Mask;

            m_segmentStencilMask = new CommandBuffer();
            m_segmentStencilMask.name = SEGMENT_CAMERA_STENCIL_MASK_BUFFER_NAME + index.ToString();

            m_camera.AddCommandBuffer( SEGMENT_CAMERA_STENCIL_MASK_EVENT, m_segmentStencilMask );
            

            m_gradeAndCopy = m_camera.GetComponent<AmplifyColorEffect>();

            if( null != m_gradeAndCopy )
            {
                m_gradeAndCopy.InitializeCommandBuffer();
            }
            else
            {
                m_segmentAccumulate = new CommandBuffer();
                m_segmentAccumulate.name = SEGMENT_CAMERA_ACCUMULATE_BUFFER_NAME + index.ToString();

                m_camera.AddCommandBuffer( SEGMENT_CAMERA_ACCUMULATE_EVENT, m_segmentAccumulate );
            }

            m_maskMesh = m_mask.Mesh;
            m_renderMaterials = renderMaterials;
        }

        public Camera Camera
        {
            get { return m_camera; }
        }

        public void RefreshForFrameStart( Camera mainCamera, Matrix4x4 relativeTransform, float depth, RenderTexture accumulationBuffer, RenderTexture offScreenBuffer )
        {
            m_camera.fieldOfView = mainCamera.fieldOfView;
            m_camera.nearClipPlane = mainCamera.nearClipPlane;
            m_camera.farClipPlane = mainCamera.farClipPlane;

            m_camera.transform.position = relativeTransform.MultiplyPoint( Vector3.zero );
            m_camera.depth = depth;
            m_camera.targetTexture = offScreenBuffer;

            m_accumulationBuffer = accumulationBuffer;
            m_offScreenBuffer = offScreenBuffer;
        }

        public void OnPassPreCull()
        {
            m_drawRoot.SetActive( true );
            RefreshForRenderStart();
        }

        public void OnPassPostRender()
        {
            m_drawRoot.SetActive( false );
        }

        public void RefreshForRenderStart()
        {
            if( null != m_gradeAndCopy )
            {
                Matrix4x4 relativeTransform = m_mask.transform.localToWorldMatrix;

                m_gradeAndCopy.RefreshCommandBuffer( m_maskMesh, relativeTransform, m_offScreenBuffer.width, m_offScreenBuffer.height, m_accumulationBuffer );
            }
            else
            {
                m_segmentAccumulate.Clear();

                Matrix4x4 relativeTransform = m_mask.transform.localToWorldMatrix;

                m_segmentAccumulate.SetGlobalFloat( Uniforms._InputTexWidth, m_accumulationBuffer.width );
                m_segmentAccumulate.SetGlobalFloat( Uniforms._InputTexHeight, m_accumulationBuffer.height );

                m_segmentAccumulate.SetGlobalTexture( Uniforms._MainTex, BuiltinRenderTextureType.CurrentActive );

                m_segmentAccumulate.SetRenderTarget( m_accumulationBuffer );

                m_segmentAccumulate.DrawMesh( m_maskMesh, relativeTransform, m_renderMaterials.SegmentAccumulateUsingMesh );

                m_segmentAccumulate.SetRenderTarget( BuiltinRenderTextureType.None );
            }
        }
    }
}
