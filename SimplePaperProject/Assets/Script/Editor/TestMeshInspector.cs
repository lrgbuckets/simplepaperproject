﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( TestMesh ))]
public class TestMeshInspector : Editor
{
    private GUILayoutOption[] m_emptyGuiOptions = new GUILayoutOption[0];
    private SerializedProperty m_size;

    void OnEnable()
    {
        m_size = serializedObject.FindProperty( "m_size" );
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField( m_size, m_emptyGuiOptions );
        serializedObject.ApplyModifiedProperties();

        if( GUILayout.Button( "Generate", m_emptyGuiOptions ) )
        {
            TestMesh tm = target as TestMesh;
            Undo.RecordObject( tm.gameObject, "Update Mesh" );
            tm.Regenerate();
        }
        
    }
}
