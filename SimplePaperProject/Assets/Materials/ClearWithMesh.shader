﻿Shader "AFoldApart/PaperProjection/ClearWithMesh"
{
	SubShader
	{
		Tags { "RenderType" = "Opaque" }

		CGINCLUDE
		struct appdata
		{
			float4 vertex : POSITION;
		};

		struct v2f
		{
			float4 pos : SV_POSITION;
		};

		v2f vert(appdata v)
		{
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			return o;
		}

		fixed4 frag(v2f i, out float out_depth : SV_Depth) : SV_Target
		{
			// https://docs.unity3d.com/Manual/SL-DepthTextures.html
#ifdef UNITY_REVERSED_Z 
			out_depth = 0;
#else
			out_depth = 1;
#endif
			return half4(0,0,0,0);
		}
		ENDCG

		Pass
		{
			ZWrite on

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}
	}
}
