// Amplify Color - Advanced Color Grading for Unity
// Copyright (c) Amplify Creations, Lda <info@amplify.pt>

using System;
using UnityEngine;

[ImageEffectTransformsToLDR]
[AddComponentMenu( "Image Effects/Amplify Color" )]
sealed public class AmplifyColorEffect : AmplifyColorBase
{
}
