﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBlit : MonoBehaviour
{
    private RenderTexture m_screenMaterial;
    private Vector2 m_textureScale;
    private Vector2 m_textureOffset;

    public void SetScreenMaterial( RenderTexture screenMaterial, Vector2 textureScale, Vector2 textureOffset )
    {
        m_screenMaterial = screenMaterial;
        m_textureScale = textureScale;
        m_textureOffset = textureOffset;
    }

    void OnRenderImage( RenderTexture source, RenderTexture destination )
    {
        Graphics.Blit( m_screenMaterial, destination, m_textureScale, m_textureOffset );
    }
}
