﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( MeshFilter ) )]
[RequireComponent( typeof( MeshRenderer ) )]
public class TestMesh : MonoBehaviour
{
    [SerializeField]
    private Vector2 m_size;

    [SerializeField]
    private Mesh m_mesh;

    public Mesh Mesh
    {
        get { return m_mesh; }
    }

    public void Regenerate()
    {
        Vector3 tlVert = new Vector3( -0.5f * m_size.x, 0.5f * m_size.y, 0f );
        Vector3 trVert = new Vector3( 0.5f * m_size.x, 0.5f * m_size.y, 0f );
        Vector3 brVert = new Vector3( 0.5f * m_size.x, -0.5f * m_size.y, 0f );
        Vector3 blVert = new Vector3( -0.5f * m_size.x, -0.5f * m_size.y, 0f );

        Vector2 tlUv = new Vector2( 0f, 1f );
        Vector2 trUv = new Vector2( 1f, 1f );
        Vector2 brUv = new Vector2( 1f, 0f );
        Vector2 blUv = new Vector2( 0f, 0f );

        Vector3[] verts = new Vector3[] { tlVert, trVert, brVert, blVert };
        Vector2[] uv = new Vector2[] { tlUv, trUv, brUv, blUv };
        int[] indices = new int[] { 0, 1, 2, 2, 3, 0 };

        m_mesh = new Mesh();
        m_mesh.vertices = verts;
        m_mesh.uv = uv;
        m_mesh.triangles = indices;

        m_mesh.RecalculateBounds();
        m_mesh.RecalculateNormals();

        MeshFilter filter = GetComponent<MeshFilter>();
        filter.sharedMesh = m_mesh;
    }
}
