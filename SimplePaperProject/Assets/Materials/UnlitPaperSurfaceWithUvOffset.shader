﻿Shader "AFoldApart/PaperProjection/UnlitPaperSurfaceWithUvOffset"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			uniform float4x4 _UvToWorld;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float4 uvWorld = mul( _UvToWorld, float4(i.uv, 0, 1) );
				float4 clipspace = mul( UNITY_MATRIX_VP, uvWorld );
				float4 screenspace = ComputeScreenPos( clipspace );
				float2 newUv = float2( screenspace.x / screenspace.w, screenspace.y / screenspace.w );

				// sample the texture
				fixed4 col = tex2D(_MainTex, newUv);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}


//Shader "AFoldApart/PaperProjection/UnlitPaperSurfaceWithUvOffset"
//{
//	Properties
//	{
//		_MainTex ("Texture", 2D) = "white" {}
//	}
//	SubShader
//	{
//		Tags { "Queue" = "Geometry" "RenderType"="Opaque" }
//		LOD 100
//
//		
//
//		Pass
//		{
//			Tags {"LightMode" = "ForwardBase"}
//			CGPROGRAM
//			#pragma vertex vert
//			#pragma fragment frag
//			#pragma multi_compile_fwdbase
//
//			#pragma fragmentoption ARB_fog_exp2
//			#pragma fragmentoption ARB_precision_hint_fastest
//			
//			#include "UnityCG.cginc"
//			#include "AutoLight.cginc"
//
//			struct v2f
//			{
//				float4 pos : SV_POSITION;
//				float2 uv : TEXCOORD0;
//				LIGHTING_COORDS(1,2)
//			};
//
//			uniform float4x4 _UvToWorld;
//			sampler2D _MainTex;
//			float4 _MainTex_ST;
//			
//			v2f vert (appdata_tan v)
//			{
//				v2f o;
//				o.pos = UnityObjectToClipPos(v.vertex);
//				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
//				TRANSFER_VERTEX_TO_FRAGMENT(o);
//				return o;
//			}
//			
//			fixed4 frag (v2f i) : SV_Target
//			{
//				float4 uvWorld = mul( _UvToWorld, float4(i.uv, 0, 1) );
//				float4 clipspace = mul( UNITY_MATRIX_VP, uvWorld );
//				float4 screenspace = ComputeScreenPos( clipspace );
//				float2 newUv = float2( screenspace.x / screenspace.w, screenspace.y / screenspace.w );
//
//				fixed atten = LIGHT_ATTENUATION(i);
//
//				fixed4 col = tex2D(_MainTex, newUv) * atten;
//				return col;
//			}
//			ENDCG
//		}
//
//		Pass {
//			Tags {"LightMode" = "ForwardAdd"}
//			Blend One One
//			CGPROGRAM
//				#pragma vertex vert
//				#pragma fragment frag
//				#pragma multi_compile_fwdadd_fullshadows
//				#pragma fragmentoption ARB_fog_exp2
//				#pragma fragmentoption ARB_precision_hint_fastest
//				
//				#include "UnityCG.cginc"
//				#include "AutoLight.cginc"
//				
//				struct v2f
//				{
//					float4	pos			: SV_POSITION;
//					float2	uv			: TEXCOORD0;
//					LIGHTING_COORDS(1,2)
//				};
//				float4 _MainTex_ST;
//				v2f vert (appdata_tan v)
//				{
//					v2f o;
//					
//					o.pos = UnityObjectToClipPos( v.vertex);
//					o.uv = TRANSFORM_TEX (v.texcoord, _MainTex).xy;
//					TRANSFER_VERTEX_TO_FRAGMENT(o);
//					return o;
//				}
//				uniform float4x4 _UvToWorld;
//				sampler2D _MainTex;
//				fixed4 frag(v2f i) : COLOR
//				{
//					float4 uvWorld = mul( _UvToWorld, float4(i.uv, 0, 1) );
//					float4 clipspace = mul( UNITY_MATRIX_VP, uvWorld );
//					float4 screenspace = ComputeScreenPos( clipspace );
//					float2 newUv = float2( screenspace.x / screenspace.w, screenspace.y / screenspace.w );
//
//					fixed atten = LIGHT_ATTENUATION(i);	// Light attenuation + shadows.
//					//fixed atten = SHADOW_ATTENUATION(i); // Shadows ONLY.
//					return tex2D(_MainTex, newUv) * atten;
//				}
//			ENDCG
//		}
//	}
//}
