﻿Shader "AFoldApart/PaperProjection/SetStencilWithMesh"
{
	Properties
	{
		_StencilRef("Stencil Ref", Float) = 0
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry-100" }

		Stencil
		{
			Ref [_StencilRef]
			Comp always
			Pass replace
		}

		CGINCLUDE
		#pragma enable_d3d11_debug_symbols
		//#pragma target 5.0

		struct appdata
		{
			float4 vertex : POSITION;
		};
		struct v2f
		{
			float4 pos : SV_POSITION;
		};
		v2f vert(appdata v)
		{
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			return o;
		}
		half4 frag(v2f i) : COLOR
		{
			return half4(1,1,0,1);
		}
		ENDCG

		Pass
		{
			ColorMask 0
			ZWrite off
			ZTest on
			Cull back

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}
	}
}