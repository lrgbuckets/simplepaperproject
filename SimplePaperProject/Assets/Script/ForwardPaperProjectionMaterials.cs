﻿using UnityEngine;

public class ForwardPaperProjectionMaterials : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField]
    private Material m_segmentAccumulateUsingMesh;

    [SerializeField]
    private Material m_blitToScreenUsingMesh;
#pragma warning restore 0649

    public Material SegmentAccumulateUsingMesh
    {
        get { return m_segmentAccumulateUsingMesh; }
    }

    public Material BlitToScreenUsingMesh
    {
        get { return m_blitToScreenUsingMesh; }
    }
}

