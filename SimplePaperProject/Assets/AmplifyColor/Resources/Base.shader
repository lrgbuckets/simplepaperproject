// Amplify Color - Advanced Color Grading for Unity
// Copyright (c) Amplify Creations, Lda <info@amplify.pt>

Shader "Hidden/Amplify Color/Base"
{
	Properties
	{
		_MainTex ( "Base (RGB)", Any ) = "" {}
		_RgbTex ( "LUT (RGB)", 2D ) = "" {}
		_LerpRgbTex ( "LerpRGB (RGB)", 2D ) = "" {}
	}

		CGINCLUDE
#pragma vertex vert
#pragma fragment frag

#include "Common.cginc"

		uniform float _InputTexWidth;
		uniform float _InputTexHeight;

		//inline fixed4 frag( UNITY_VPOS_TYPE screenPos : VPOS ) : SV_Target
		//{
		//	float2 uv = float2(screenPos.x / _InputTexWidth, (float)screenPos.y / _InputTexHeight);
		//	fixed4 col = tex2D(_MainTex, uv);
		//	return col;
		//}

		inline float4 update_screen_position( float4 originalScreenPos )
		{
			return float4(originalScreenPos.x / _InputTexWidth, originalScreenPos.y / _InputTexHeight, 0, 0 );
		}

		inline float4 frag_ldr_gamma( v2f i, const bool mobile )
		{
			i.screenPos = update_screen_position(i.screenPos);
			i.uv01Stereo = update_screen_position(i.uv01Stereo);
			init_frag( i );
			float4 color = fetch_process_ldr_gamma( i, mobile );
			color = apply( color, mobile );
			return output_ldr_gamma( color );
		}

		inline float4 frag_ldr_linear( v2f i, const bool mobile )
		{
			i.screenPos = update_screen_position(i.screenPos);
			i.uv01Stereo = update_screen_position(i.uv01Stereo);
			init_frag( i );
			float4 color = fetch_process_ldr_linear( i, mobile );
			color = apply( color, mobile );
			return output_ldr_linear( color );
		}

		inline float4 frag_hdr_gamma( v2f i, const bool mobile, const bool dithering, const int tonemapper )
		{
			i.screenPos = update_screen_position(i.screenPos);
			i.uv01Stereo = update_screen_position(i.uv01Stereo);
			init_frag( i );
			float4 color = fetch_process_hdr_gamma( i, mobile, dithering, tonemapper );
			color = apply( color, mobile );
			return output_hdr_gamma( color );
		}

		inline float4 frag_hdr_linear( v2f i, const bool mobile, const bool dithering, const int tonemapper )
		{
			
			//init_frag( i );
			//i.screenPos = update_screen_position(i.screenPos);
			//i.uv01Stereo = update_screen_position(i.uv01Stereo);
			//float4 color = fetch_process_hdr_linear( i, mobile, dithering, tonemapper );
			//color = apply( color, mobile );
			//return output_hdr_linear( color );

			float2 uv = float2( (float)i.pos.x / _InputTexWidth, (float)i.pos.y / _InputTexHeight );

			float4 color = tex2D(_MainTex, uv);

			// fetch
			//float4 color = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_MainTex, i.uv01Stereo.xy);

			// tonemap
			color.rgb = tonemap(tonemapper, color.rgb);

			// convert to gamma
			color = to_srgb(color);

			// dither
			if (dithering)
			{
				color.rgb += screen_space_dither(i.screenPos);
			}

			// clamp
			if (mobile)
			{
				color.rgb = clamp((0.0).xxx, (0.999).xxx, color.rgb); // dev/hw compatibility
			}
			else
			{
				color.rgb = saturate(color.rgb);
			}
			
			color = apply( color, mobile );
			return output_hdr_linear( color );
		}
	ENDCG

	Subshader
	{
		ZTest Always Cull Off ZWrite Off Blend Off Fog { Mode off }

		// -- QUALITY NORMAL --------------------------------------------------------------
		// 0 => LDR GAMMA
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_ldr_gamma( i, false ); } ENDCG }

		// 1 => LDR LINEAR
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_ldr_linear( i, false ); } ENDCG }

		// 2-5 => HDR GAMMA / DITHERING: OFF
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_gamma( i, false, false, TONEMAPPING_DISABLED ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_gamma( i, false, false, TONEMAPPING_PHOTO ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_gamma( i, false, false, TONEMAPPING_HABLE ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_gamma( i, false, false, TONEMAPPING_ACES ); } ENDCG }

		// 6-9 => HDR GAMMA / DITHERING: ON
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_gamma( i, false, true, TONEMAPPING_DISABLED ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_gamma( i, false, true, TONEMAPPING_PHOTO ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_gamma( i, false, true, TONEMAPPING_HABLE ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_gamma( i, false, true, TONEMAPPING_ACES ); } ENDCG }

		// 10-13 => HDR LINEAR / DITHERING: OFF
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_linear( i, false, false, TONEMAPPING_DISABLED ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_linear( i, false, false, TONEMAPPING_PHOTO ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_linear( i, false, false, TONEMAPPING_HABLE ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_linear( i, false, false, TONEMAPPING_ACES ); } ENDCG }

		// 14-17 => HDR LINEAR / DITHERING: ON
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_linear( i, false, true, TONEMAPPING_DISABLED ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_linear( i, false, true, TONEMAPPING_PHOTO ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_linear( i, false, true, TONEMAPPING_HABLE ); } ENDCG }
		Pass{ CGPROGRAM float4 frag( v2f i ) : SV_Target{ return frag_hdr_linear( i, false, true, TONEMAPPING_ACES ); } ENDCG }

		// -- QUALITY MOBILE --------------------------------------------------------------
		// 18 => LDR GAMMA
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_ldr_gamma( i, true ); } ENDCG }

		// 19 => LDR LINEAR
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_ldr_linear( i, true ); } ENDCG }

		// 20-23 => HDR GAMMA / DITHERING: OFF
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_gamma( i, true, false, TONEMAPPING_DISABLED ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_gamma( i, true, false, TONEMAPPING_PHOTO ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_gamma( i, true, false, TONEMAPPING_HABLE ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_gamma( i, true, false, TONEMAPPING_ACES ); } ENDCG }

		// 24-27 => HDR GAMMA / DITHERING: ON
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_gamma( i, true, true, TONEMAPPING_DISABLED ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_gamma( i, true, true, TONEMAPPING_PHOTO ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_gamma( i, true, true, TONEMAPPING_HABLE ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_gamma( i, true, true, TONEMAPPING_ACES ); } ENDCG }

		// 28-31 => HDR LINEAR / DITHERING: OFF
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_linear( i, true, false, TONEMAPPING_DISABLED ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_linear( i, true, false, TONEMAPPING_PHOTO ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_linear( i, true, false, TONEMAPPING_HABLE ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_linear( i, true, false, TONEMAPPING_ACES ); } ENDCG }

		// 32-35 => HDR LINEAR / DITHERING: ON
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_linear( i, true, true, TONEMAPPING_DISABLED ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_linear( i, true, true, TONEMAPPING_PHOTO ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_linear( i, true, true, TONEMAPPING_HABLE ); } ENDCG }
		Pass { CGPROGRAM float4 frag( v2f i ) : SV_Target { return frag_hdr_linear( i, true, true, TONEMAPPING_ACES ); } ENDCG }
	}

	Fallback Off
}
