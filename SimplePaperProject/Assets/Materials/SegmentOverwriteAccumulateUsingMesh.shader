﻿Shader "AFoldApart/PaperProjection/SegmentOverwriteAccumulateUsingMesh"
{
	SubShader
	{
		Tags{ "Queue" = "Overlay" "PreviewType" = "Plane" }

		CGINCLUDE
		#pragma enable_d3d11_debug_symbols
		#pragma target 3.0
		#include "UnityCG.cginc"

		void vert(
			float4 vertex : POSITION, // vertex position input
			out float4 outpos : SV_POSITION // clip space position output
		)
		{
			outpos = UnityObjectToClipPos(vertex);
		}

		sampler2D _MainTex;
		uniform float _InputTexWidth;
		uniform float _InputTexHeight;

		fixed4 frag( UNITY_VPOS_TYPE screenPos : VPOS ) : SV_Target
		{
			float2 uv = float2( screenPos.x / _InputTexWidth, (float)screenPos.y / _InputTexHeight );
			fixed4 col = tex2D( _MainTex, uv );
			return col;
		}
		ENDCG

		Pass
		{
			Cull back

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}
	}
}
