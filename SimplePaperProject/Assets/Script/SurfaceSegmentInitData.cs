﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering;

[System.Serializable]
public class SurfaceSegmentInitData
{
    [SerializeField]
    private Camera m_camera;

    [SerializeField]
    private GameObject m_drawRoot;

    [SerializeField]
    private TestMesh m_mask;

    public Camera Camera
    {
        get { return m_camera; }
    }

    public GameObject DrawRoot
    {
        get { return m_drawRoot; }
    }

    public TestMesh Mask
    {
        get { return m_mask; }
    }
}